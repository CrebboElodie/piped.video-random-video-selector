package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type Channel struct {
	URL  string `json:"url"`
	Name string `json:"name"`
}

type Subscriptions struct {
	AppVersion        string    `json:"app_version"`
	AppVersionInt     int       `json:"app_version_int"`
	SubscriptionItems []Channel `json:"subscriptions"`
}

type Video struct {
	Title    string `json:"title"`
	VideoURL string `json:"video_url"`
	Uploader string `json:"uploader"`
}

type SearchResult struct {
	Items []struct {
		ID      struct {
			VideoID string `json:"videoId"`
		} `json:"id"`
		Snippet struct {
			Title string `json:"title"`
		} `json:"snippet"`
	} `json:"items"`
}

func getRandomVideo(channels []Channel) (*Video, error) {
	rand.Seed(time.Now().UnixNano())
	randomChannel := channels[rand.Intn(len(channels))]
	channelID := randomChannel.URL[len("https://www.youtube.com/channel/"):]
	apiKey := "YOUR_YOUTUBE_API_KEY" // Replace with your own YouTube API key

	// Make a request to the YouTube Data API to get the latest videos from all channels
	apiURL := fmt.Sprintf("https://www.googleapis.com/youtube/v3/search?key=%s&channelId=%s&part=snippet,id&order=date&maxResults=50", apiKey, channelID)
	response, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var searchResult SearchResult
	err = json.Unmarshal(data, &searchResult)
	if err != nil {
		return nil, err
	}

	if len(searchResult.Items) > 0 {
		randomVideo := searchResult.Items[rand.Intn(len(searchResult.Items))]
		videoID := randomVideo.ID.VideoID
		videoTitle := randomVideo.Snippet.Title
		videoURL := fmt.Sprintf("https://piped.video/watch?v=%s", videoID)

		return &Video{Title: videoTitle, VideoURL: videoURL, Uploader: randomChannel.Name}, nil
	}

	return nil, fmt.Errorf("no videos found for channel: %s", randomChannel.Name)
}

func main() {
	file, err := ioutil.ReadFile("subscriptions.json")
	if err != nil {
		log.Fatal(err)
	}

	var subscriptions Subscriptions
	err = json.Unmarshal(file, &subscriptions)
	if err != nil {
		log.Fatal(err)
	}

	// Extract the channel URLs from the JSON data
	channelURLs := subscriptions.SubscriptionItems

	// Get a random video
	video, err := getRandomVideo(channelURLs)
	if err != nil {
		log.Fatal(err)
	}

	// Print the random video information
	fmt.Println("Random Video:")
	fmt.Println("Title:", video.Title)
	fmt.Println("URL:", video.VideoURL)
	fmt.Println("Uploader:", video.Uploader)
}

