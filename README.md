**Random YouTube Video Selector**

This program selects a random YouTube video uploaded by a channel specified in a JSON file. It utilizes the YouTube Data API to retrieve the latest videos from the channels and selects a random video from the results.

**Installation:**

1. Make sure you have Go installed on your system. You can download and install it from the official Go website: https://golang.org/

2. Obtain a YouTube Data API key:
   - Go to the Google Cloud Console: https://console.cloud.google.com/
   - Create a new project or select an existing project.
   - Enable the YouTube Data API for your project.
   - Create credentials and generate an API key.

3. Clone or download the program files to your local system.

4. Open the `main.go` file in a text editor.

5. Replace the placeholder `'YOUR_YOUTUBE_API_KEY'` with your actual YouTube Data API key obtained in step 2.

**Preparing the JSON File:**

1. Prepare the JSON file:
   - Create a JSON file (`subscriptions.json`) with the following format:
     ```json
     {
       "app_version": "",
       "app_version_int": 0,
       "subscriptions": [
         {
           "url": "https://www.youtube.com/channel/CHANNEL_1_ID",
           "name": "CHANNEL_1",
           "service_id": 0
         },
         {
           "url": "https://www.youtube.com/channel/CHANNEL_2_ID",
           "name": "CHANNEL_2",
           "service_id": 0
         },
         ...
       ]
     }
     ```
   - Replace `'CHANNEL_X_ID'` with the actual YouTube channel IDs you want to include in the selection.

2. Save the `subscriptions.json` file in the same directory as the program files.

OR:

1. Export subscriptions from Piped.Video:
   - Go to Piped.Video: https://piped.video/
   - Log in to your account or create a new account if needed.
   - Navigate to your subscriptions page.
   - Export your subscriptions as a JSON file.
   - Place the exported file `subscriptions.json` in the same directory as the program files.

**Usage:**

1. Open a command prompt or terminal and navigate to the program directory.

2. Build the program:
   ```
   go build
   ```

3. Run the program:
   ```
   ./program-name
   ```
   Replace `program-name` with the name of the generated executable file.

4. The program will select a random video from one of the specified channels and display its details, including the title, URL, and uploader.

**Important Note:**

- Ensure that you have a stable internet connection to access the YouTube Data API and retrieve the latest videos.
- Make sure to comply with the usage limits and quotas of the YouTube Data API to avoid any issues.